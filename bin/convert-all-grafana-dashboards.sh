#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

rm -rf output/

for file in $(find . -name '*.json'); do
  echo ${file}
  dir=$(dirname ${file})
  mkdir -p output/$dir
  ${SCRIPT_DIR}/transform-exported-grafana-dashboard.js ${file} > output/${file}
done
